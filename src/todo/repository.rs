#![allow(proc_macro_derive_resolution_fallback)]
use anyhow::{anyhow, Result};
use crate::todo::{Todo, InsertableTodo};
use crate::mongo_connection::Conn;
use mongodb::bson::{doc, oid::ObjectId};
use mongodb::{bson,
    results::DeleteResult,
};

const DATABASE: &str = "test";
const COLLECTION: &str = "todos";

pub fn all(connection: &Conn) -> Result<Vec<Todo>> {
    let cursor = connection.database(DATABASE).collection(COLLECTION).find(None, None).unwrap();

    cursor
        .map(|result| match result {
            Ok(doc) => match bson::from_bson(bson::Bson::Document(doc)) {
                Ok(result_model) => Ok(result_model),
                Err(_) => Err(anyhow!(String::from("could not read BSON"))),
            },
            Err(err) => Err(anyhow!(err)),
        })
        .collect::<Result<Vec<Todo>>>()
}

pub fn get(id: ObjectId, connection: &Conn) -> Result<Option<Todo>> {
    match connection.database(DATABASE)
        .collection(COLLECTION)
        .find_one(Some(doc! {"_id": id}), None)
    {
        Ok(db_result) => match db_result {
            Some(result_doc) => match bson::from_bson(bson::Bson::Document(result_doc)) {
                Ok(result_model) => Ok(Some(result_model)),
                Err(_) => Err(anyhow!(String::from(
                    "Failed to create reverse BSON",
                ))),
            },
            None => Ok(None),
        },
        Err(err) => Err(anyhow!(err)),
    }
}

pub fn insert(todos: Todo, connection: &Conn) -> Result<ObjectId> {
    let insertable = InsertableTodo::from_todo(todos);
    match bson::to_bson(&insertable) {
        Ok(model_bson) => match model_bson {
            bson::Bson::Document(model_doc) => {
                let result = connection
                    .database(DATABASE)
                    .collection(COLLECTION)
                    .insert_one(model_doc, None);
                // result.b();
                match result {
                    Ok(res) => match bson::from_bson(res.inserted_id) {
                        Ok(r) => Ok(r),
                        Err(err) => Err(anyhow!(err)),
                    }
                    Err(err) => Err(anyhow!(err)),
                }
            }
            _ => Err(anyhow!(String::from(
                "Failed to create Document",
            ))),
        },
        Err(_) => Err(anyhow!(String::from("Failed to create BSON"))),
    }
}

pub fn update(id: ObjectId, todos: Todo, connection: &Conn) -> Result<Todo> {
    let mut new_todo = todos;
    new_todo.id = Some(id.clone());
    match bson::to_bson(&new_todo) {
        Ok(model_bson) => match model_bson {
            bson::Bson::Document(model_doc) => {
                match connection
                    .database(DATABASE)
                    .collection(COLLECTION).replace_one(
                    doc! {"_id": id},
                    model_doc,
                    None,
                ) {
                    Ok(_) => Ok(new_todo),
                    Err(err) => Err(anyhow!(err)),
                }
            }
            _ => Err(anyhow!(String::from(
                "Failed to create Document",
            ))),
        },
        Err(_) => Err(anyhow!(String::from("Failed to create BSON"))),
    }
}

pub fn delete(id: ObjectId, connection: &Conn) -> Result<DeleteResult> {
    connection
        .database(DATABASE)
        .collection(COLLECTION)
        .delete_one(doc! {"_id": id}, None)
        .map_err(|e| anyhow!(e))
}

pub fn delete_all(connection: &Conn) -> Result<()> {
    connection
        .database(DATABASE)
        .collection(COLLECTION).drop(None)
        .map_err(|e| anyhow!(e))
}
