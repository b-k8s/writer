use crate::todo::{self, Todo};
use crate::mongo_connection::Conn;

use anyhow::Result;
use mongodb::bson::{doc, oid::ObjectId};
use rocket::http::Status;
use rocket_contrib::json::Json;

use rocket::{get, post, put, delete};

#[get("/")]
pub fn all(connection: Conn) -> Result<Json<Vec<Todo>>, Status> {
    match todo::repository::all(&connection) {
        Ok(res) => Ok(Json(res)),
        Err(_) => Err(Status::InternalServerError),
    }
}

#[get("/<id>")]
pub fn get(id: String, connection: Conn) -> Result<Json<Todo>, Status> {
    match ObjectId::with_string(&String::from(&id)) {
        Ok(res) => match todo::repository::get(res, &connection) {
            Ok(res) => Ok(Json(res.unwrap())),
            Err(_) => Err(Status::InternalServerError),
        },
        Err(_) => Err(Status::NotFound),
    }
}

#[post("/", format = "application/json", data = "<todos>")]
pub fn post(todos: Json<Todo>, connection: Conn) -> Result<Json<ObjectId>, Status> {
    match todo::repository::insert(todos.into_inner(), &connection) {
        Ok(res) => Ok(Json(res)),
        Err(_) => Err(Status::InternalServerError),
    }
}

#[put("/<id>", format = "application/json", data = "<todos>")]
pub fn put(id: String, todos: Json<Todo>, connection: Conn) -> Result<Json<Todo>, Status> {
    match ObjectId::with_string(&String::from(&id)) {
        Ok(res) => match todo::repository::update(res, todos.into_inner(), &connection) {
            Ok(res) => Ok(Json(res)),
            Err(_) => Err(Status::InternalServerError),
        },
        Err(_) => Err(Status::InternalServerError),
    }
}

#[delete("/<id>")]
pub fn delete(id: String, connection: Conn) -> Result<Json<String>, Status> {
    match ObjectId::with_string(&String::from(&id)) {
        Ok(res) => match todo::repository::delete(res, &connection) {
            Ok(_) => Ok(Json(id)),
            Err(_) => Err(Status::InternalServerError),
        },
        Err(_) => Err(Status::InternalServerError),
    }
}

#[delete("/")]
pub fn delete_all(connection: Conn) -> Result<Json<bool>, Status> {
    match todo::repository::delete_all(&connection) {
        Ok(_) => Ok(Json(true)),
        Err(_) => Err(Status::InternalServerError),
    }
}
