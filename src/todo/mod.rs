pub mod handler;
pub mod repository;
use mongodb::bson;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Todo {
    #[serde(rename = "_id")]  // Use MongoDB's special primary key field name when serializing
    pub id: Option<bson::oid::ObjectId>,
    pub done: bool,
    pub title: String,
    pub notes: Option<String>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct InsertableTodo {
    pub done: bool,
    pub title: String,
    pub notes: Option<String>,
}

impl InsertableTodo {
    fn from_todo(t: Todo) -> InsertableTodo {
        InsertableTodo {
            done: t.done,
            title: t.title,
            notes: t.notes,
        }
    }
}
