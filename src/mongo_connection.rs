use dotenv::dotenv;
use mongodb::sync::Client;
use rocket::request::{self, FromRequest};
use rocket::{Outcome, Request, State};
use std::env;
use std::ops::Deref;

// type Pool = r2d2::Pool<MongodbConnectionManager>;

// pub struct Conn(pub PooledConnection<MongodbConnectionManager>);
pub struct Conn(pub Client);

/// Create shared state mongodb client (with embedded connection pool)
pub fn init_db_conn() -> Client {
    dotenv().ok();
    let db_conn_string = env::var("MONGO_DB_CONN_STR").expect("MONGO_DB_CONN_STR env var must be set");
    let client = Client::with_uri_str(&db_conn_string);

    match client {
        Ok(client) => client,
        //TODO should not panic, but should retry!!!
        Err(_) => panic!("Error: failed to connect to mongodb!"),
    }
}

/// Create a implementation of FromRequest so Conn can be provided at every api endpoint
impl<'a, 'r> FromRequest<'a, 'r> for Conn {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Conn, ()> {
        let state = request.guard::<State<Client>>()?;
        Outcome::Success(Conn(state.inner().clone()))
    }
}

/// When Conn is dereferencd, return the mongo connection.
impl Deref for Conn {
    type Target = Client;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
