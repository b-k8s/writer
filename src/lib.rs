#![feature(proc_macro_hygiene, decl_macro)]
use rocket::{catch, catchers, routes, Request, Rocket};
use dotenv::dotenv;

mod mongo_connection;
pub mod todo;

#[catch(500)]
fn internal_error() -> &'static str {
    "Whoops! Looks like we messed up."
}

#[catch(400)]
fn not_found(req: &Request) -> String {
    format!("I couldn't find '{}'. Try something else?", req.uri())
}

pub fn rocket() -> Rocket {
    dotenv().ok();
    rocket::ignite()
        .register(catchers![internal_error, not_found])
        .manage(mongo_connection::init_db_conn())
        .mount(
            "/todo",
            routes![
                todo::handler::all,
                todo::handler::get,
                todo::handler::post,
                todo::handler::put,
                todo::handler::delete,
                todo::handler::delete_all
            ],
        )
}

